﻿namespace Sitecore.FxCop.Security
{
    using System;
    using Microsoft.FxCop.Sdk;

    internal sealed class DontUseSecurityDisabler : SitecoreBaseRule
    {
        public DontUseSecurityDisabler()
            : base("DontUseSecurityDisabler")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            var method = member as Method;
            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitConstruct(Construct construct)
        {
            base.VisitConstruct(construct);

            Method method = (Method)((MemberBinding)construct.Constructor).BoundMember;
            if (method.FullName.Equals("Sitecore.SecurityModel.SecurityDisabler.#ctor", StringComparison.OrdinalIgnoreCase))
            {
                this.Problems.Add(new Problem(this.GetResolution(method), construct));
            }
        }
    }
}
