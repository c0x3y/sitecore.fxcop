﻿namespace Sitecore.FxCop.Fields
{
    using Microsoft.FxCop.Sdk;
    using System;

    internal sealed class AccessRawFieldValueProperly : SitecoreBaseRule
    {
        public AccessRawFieldValueProperly() : base("AccessRawFieldValueProperly")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            var method = member as Method;
            if (method != null)
            {
                VisitStatements(method.Body.Statements);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall methodCall)
        {
            var memberBinding = methodCall.Callee as MemberBinding;
            if (memberBinding != null)
            {
                var methodCalled = memberBinding.BoundMember as Method;
                if (methodCalled != null)
                {
                    if (methodCalled.FullName == "Sitecore.Data.Fields.Field.get_Value")
                    {
                        Problems.Add(new Problem(GetResolution(), methodCall));
                    }
                }
            }
        }
    }
}