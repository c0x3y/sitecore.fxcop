﻿namespace Sitecore.FxCop.Database
{
    using System;
    using Microsoft.FxCop.Sdk;

    internal sealed class DontUseGetDatabase : SitecoreBaseRule
    {
        public DontUseGetDatabase()
            : base("DontUseGetDatabase")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            Method method = member as Method;
            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall call)
        {
            base.VisitMethodCall(call);

            Method targetMethod = (Method)((MemberBinding)call.Callee).BoundMember;

            if (targetMethod.DeclaringType.FullName.Equals("Sitecore.Data.Database", StringComparison.Ordinal) && targetMethod.Name.Name.Equals("GetDatabase", StringComparison.Ordinal))
            {
                this.Problems.Add(new Problem(this.GetResolution(), call));
            }
        }
    }
}
