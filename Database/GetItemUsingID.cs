﻿namespace Sitecore.FxCop.Database
{
    using System;
    using System.Text.RegularExpressions;
    using Microsoft.FxCop.Sdk;

    internal sealed class GetItemUsingID : SitecoreBaseRule
    {
        public GetItemUsingID() : base("GetItemUsingID")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            Method method = member as Method;
            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitMethodCall(MethodCall call)
        {
            base.VisitMethodCall(call);

            Method targetMethod = (Method)((MemberBinding)call.Callee).BoundMember;

            // Only trigger when the GetItem method is called
            if (targetMethod.DeclaringType.FullName.Equals("Sitecore.Data.Database", StringComparison.Ordinal) &&
                targetMethod.Name.Name.Equals("GetItem", StringComparison.Ordinal))
            {
                // No matter how many parameters get passed in, we will always need the 0th
                Expression endResponseOperand = call.Operands[0];

                CheckValue(call, endResponseOperand);
            }
        }

        /// <summary>
        /// Checks the value for an expression
        /// </summary>
        /// <param name="call"></param>
        /// <param name="expression"></param>
        /// <remarks>Currently only will create ONE FxCop issue per method.</remarks>
        private void CheckValue(MethodCall call, Expression expression)
        {
            string value = string.Empty;

            if (expression.NodeType == NodeType.Literal)
            {
                value = ((Literal)expression).Value.ToString();
            }
            else if (expression.NodeType == NodeType.Construct)
            {
                var operands = ((Microsoft.FxCop.Sdk.Construct)expression).Operands;

                if (operands[0].NodeType == NodeType.Literal)
                {
                    value = ((Literal)operands[0]).Value.ToString();
                }
            }

            // Check whether GetItem gets called with a GUID
            var match = Regex.IsMatch(value, @"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b", RegexOptions.IgnoreCase);
            if (!match)
            {
                this.Problems.Add(new Problem(this.GetResolution(value), call));
            }
        }
    }
}
